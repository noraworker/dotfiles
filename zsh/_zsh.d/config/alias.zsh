# 
# ZSHの環境設定
#

# Global Alias
alias -g G='| grep'
alias -g GI='| grep -i'

# alias
alias vi=vim
alias virc="vim ~/.vimrc"
alias vish="vim +VimShell"
alias vizshrc="vim ~/.zshrc"
